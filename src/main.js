import {toFormatted, findNumberInResponse} from './utils.js';

import './main.scss';

const OBJECT_PROPERTIES_TO_CHECK = [
  'signatureCount',
  'total_count',
  'totalSubmissionProgress',
  'count',
  'amount',
  'amountAllOpportunities'
];

function isElementInViewport(element) {
  const rect = element.getBoundingClientRect();

  return (
    rect.top >= 0 &&
    rect.left >= 0 &&
    rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
    rect.right <= (window.innerWidth || document.documentElement.clientWidth)
  );
}

function possiblyShow(container, currentNumber, nextGoal, show = true) {
  if (show) {
    window.setTimeout(() => {
      container.getElementsByClassName('progress-bar')[0].style.opacity = 1;
      container.getElementsByClassName(
        'progress-bar__progress__bar progress-bar__progress__bar--current'
      )[0].style.width = `${show ? Math.min(currentNumber / nextGoal * 100, 100) : 0}%`;
    }, 250);
  }
}

const render = (currentNumber, offset, goals, labelGoal, label, language, container, disableScrollAnimation) => {
  goals.sort((a, b) => a - b);

  if (isNaN(currentNumber)) {
    return;
  }

  currentNumber += offset;

  const goalsAboveCurrentNumber = goals.filter(goal => goal * 0.8 > currentNumber);
  const nextGoal = goalsAboveCurrentNumber.length ? goalsAboveCurrentNumber[0] : goals[goals.length - 1];

  container.style.display = 'block';
  const formattedValue = `<strong>${toFormatted(language, currentNumber)}</strong>`;
  const formattedLabel = label.includes('%s') ? label.replace('%s', formattedValue) : formattedValue + ' ' + label;

  container.innerHTML = `
    <div class="progress-bar" style="opacity: ${disableScrollAnimation ? '1' : '0'}">
      ${labelGoal
    ? `<div class="progress-bar__goal">
      ${labelGoal && labelGoal.replace('{goal}', `<strong>${toFormatted(language, nextGoal)}</strong>`)}
    </div>`
    : ''
}
    <div class="progress-bar__progress">
      <div class="progress-bar__progress__bar progress-bar__progress__bar--target"></div>
      <div class="progress-bar__progress__bar progress-bar__progress__bar--current"></div>
    </div>
    <div class="progress-bar__text">
      ${formattedLabel}
    </div>
  </div>`;

  possiblyShow(container, currentNumber, nextGoal, isElementInViewport(container) || disableScrollAnimation);

  if (!disableScrollAnimation) {
    document.addEventListener('scroll', () => {
      possiblyShow(container, currentNumber, nextGoal, isElementInViewport(container));
    });
  }
};

/**
 * @description Renders a progressbar, which raises linearly to goal - (100 * goalOffsetFactor)%.
 * After that the goal will be always (100 * goalOffsetFactor)% above the current count
 * @param {DOMObject} container - Container to render the progress bar in
 * @param {string} endpoint - URL to fetch the data from by GET
 * @param {string} pathToResult - Where to find the number to use inside the response
 * @param {IETF BCP 47 language tag} language - Define language to format number properly
 * @param {string} label - text to show after the counter
 * @param {string} labelGoal - text to show after the counter describing the goal
 * @param {number} offset
 * @param {number[]} goals
 */
export default (container, endpoint, pathToResult, label, labelGoal, language = null, offset = 0, goals = [
  1000, 2000, 5000, 10000, 15000, 25000, 50000, 100000, 250000, 500000, 1000000, 5000000, 10000000
], disableScrollAnimation = false) => {
  const htmlLanguage = document.querySelector('html').getAttribute('lang');
  const parsedOffset = parseInt(offset, 10);

  container.style.display = 'none';

  if (!language) {
    language = htmlLanguage || 'en-US';
  }

  if (endpoint) {
    window.fetch(endpoint, {
      method: 'GET'
    }).then(response => {
      response.json().then(result => {
        render(
          findNumberInResponse(result, pathToResult ? [pathToResult] : OBJECT_PROPERTIES_TO_CHECK),
          isNaN(parsedOffset) ? 0 : parsedOffset,
          goals,
          labelGoal,
          label,
          language,
          container,
          disableScrollAnimation
        );
      });
    });
  } else {
    render(isNaN(parsedOffset) ? 0 : parsedOffset, 0, goals, labelGoal, label, language, container, disableScrollAnimation);
  }
};
