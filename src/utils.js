function toFormatted(language, number) {
  if (Intl && Intl.NumberFormat) {
    return new Intl.NumberFormat(language).format(number);
  } else {
    return number;
  }
}

function findNumberInResponse(response, propertiesToCheck) {
  let number = null;

  propertiesToCheck.forEach(property => {
    if (response[property]) {
      number = parseInt(response[property], 10);
    }
  });

  return number;
}

export {toFormatted, findNumberInResponse};
